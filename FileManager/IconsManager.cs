﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager
{
    public class IconsManager
    {
        private const string FileManagerIconsDirName = @"FileManagerIcons";
        public static string GetImageSource(FileInfo file)
        {
            string FileManagerIconsDir = Path.Combine(Path.GetTempPath(), FileManagerIconsDirName);
            if (!Directory.Exists(FileManagerIconsDir))
                Directory.CreateDirectory(FileManagerIconsDir);
            string imageSource = Path.Combine(FileManagerIconsDir, file.Extension.ToUpperInvariant());
            try
            {
                if (!File.Exists(imageSource))
                    System.Drawing.Icon.ExtractAssociatedIcon(file.FullName).ToBitmap().Save(imageSource);
            }
            catch { }
            return imageSource;
           
        }

        public static string GetImageSource(DirectoryInfo dir)
        {
            string FileManagerIconsDir = Path.Combine(Path.GetTempPath(), FileManagerIconsDirName);
            if (!Directory.Exists(FileManagerIconsDir))
                Directory.CreateDirectory(FileManagerIconsDir);
            string imageSource = Path.Combine(FileManagerIconsDir, "folder1.ico");
            return imageSource;
        }
    }
}
