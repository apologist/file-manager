﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager
{
    public class FileElement : Element
    {
        private FileInfo _file;
        private bool _simulated;

        public FileElement(FileInfo file)
        {
            _file = file;
        }

        #region Properties

        public FileInfo File
        {
            get { return _file; }
        }

        public override bool Simulated
        {
            get { return _simulated; }
            set { _simulated = value; }
        }

        public override bool Available
        {
            get { return PathExists(_file.FullName); }
        }

        public override bool IsEditable
        {
            get { return true; }
        }

        public override string ImageSource
        {
            get { return IconsManager.GetImageSource(_file); }
        }

        public override string Name
        {
            get { return _file.Name; }
        }

        public override string Extension
        {
            get { return _file.Extension; }
        }

        public override string Size
        {
            get { return _file.Length.ToString("N0"); }
        }

        public override FileAttributes Attributes
        {
            get { return _file.Attributes; }
        }

        public override DateTime Date
        {
            get { return _file.LastWriteTimeUtc; }
        }

        #endregion

        public void CopyTo(string destFileName, bool overwrite = false)
        {
            _file.CopyTo(destFileName, overwrite);
        }

        public void MoveTo(string destFileName)
        {
            _file.MoveTo(destFileName);
        }

        public void Delete()
        {
            _file.Delete();
        }
    }
}
