﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpf.Core;
using FileManager.Interfaces;
using SimpleTextEditor;


namespace FileManager
{
    public class PanelManager : ActiveManager
    {
        private DXTabControl tabControl1;
        private DXTabControl tabControl2;

        private Dictionary<object, bool> _activityDictionary;

        TabManager tabManager1;
        TabManager tabManager2;

        public PanelManager(DXTabControl tabControl1, DXTabControl tabControl2)
        {
            this.tabControl1 = tabControl1;
            this.tabControl2 = tabControl2;

            tabManager1 = new TabManager(tabControl1, this);
            tabManager2 = new TabManager(tabControl2, this);

            _activityDictionary = new Dictionary<object, bool>();

            _activityDictionary.Add(tabManager1, false);
            _activityDictionary.Add(tabManager2, false);

            tabManager1.SetOnStartTabs();
            tabManager2.SetOnStartTabs();
            
        }

        public bool IsActive(object obj)
        {
            if (_activityDictionary.Keys.Contains(obj))
                return _activityDictionary[obj];
            return false;
        }

        public void Activate(object obj)
        {
            foreach(var key in _activityDictionary.Keys.ToList())
                _activityDictionary[key] = false;
            _activityDictionary[obj] = true;
 
        }

        public TabManager TabManager1
        {
            get { return tabManager1; }
        }

        public TabManager TabManager2
        {
            get { return tabManager2; }
        }

        public TabManager GetActiveTabManager()
        {
            return _activityDictionary.Keys.OfType<TabManager>().Where(k => _activityDictionary[k] == true).FirstOrDefault();   
        }

        public TabManager GetInActiveTabManager()
        {
            return _activityDictionary.Keys.OfType<TabManager>().Where(k => _activityDictionary[k] == false).FirstOrDefault() ?? tabManager1;

        }

        public void RefreshActiveViews()
        {
            try
            {
                ActiveElementsControl.RefreshData();
                InActiveElementsControl.RefreshData();
            }
            catch { }
        }

        public TabManager ActiveTabManager
        {
            get
            {
                if (tabManager1.IsActive)
                    return tabManager1;
                else if (tabManager2.IsActive)
                    return tabManager2;
                else
                    return GetActiveTabManager();
            }
        }

        public TabManager InActiveTabManager
        {
            get
            {
                if (tabManager1.IsActive)
                    return tabManager2;
                else if (tabManager2.IsActive)
                    return tabManager1;
                else
                    return GetInActiveTabManager();
            }
        }

        public ElementsControl ActiveElementsControl
        {
            get
            {
                if (ActiveTabManager != null)
                    return ActiveTabManager.ActiveElementsControl;
                else return null;
            }
        }

        public ElementsControl InActiveElementsControl
        {
            get
            {
                if (InActiveTabManager != null)
                    return InActiveTabManager.ActiveElementsControl;
                else return null;
            }
        }

        public void SwitchActivePanel()
        {
            if (tabManager1.ActiveView.IsFocused)
                tabManager2.ActiveView.Focus();
            else if (tabManager2.ActiveView.IsFocused)
                tabManager1.ActiveView.Focus();
            else tabManager1.ActiveView.Focus();
        }
    }
}
