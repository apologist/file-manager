﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Xpf.Core;
using System.Windows.Controls;
using FileManager.Interfaces;

namespace FileManager
{
    public class TabManager
    {
        private static readonly string DEFAULT_TAB_PATH = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

        private DXTabControl _tabControl;
        private ActiveManager _activeManager;

        public TabManager(DXTabControl tabControl, ActiveManager activeManager)
        {
            _tabControl = tabControl;
            _activeManager = activeManager;
            _tabControl.GotFocus += (s, e) =>
                {
                    _activeManager.Activate(this);
                };
        }

        public bool IsActive
        {
            get { return ActiveView.IsFocused; }
        }

        public DXTabControl TabControl
        {
            get { return _tabControl; }
        }

        public ListView ActiveView
        {
            get 
            {
                
                var control = _tabControl.SelectedItemContent as ElementsControl;
                if (control != null)
                    return control.ElementsListView;
                else return null;
            }
        }

        public ElementsControl ActiveElementsControl
        {
            get
            {
                var control = _tabControl.SelectedItemContent as ElementsControl;
                if (control != null)
                    return control;
                else return null;
            }
        }

        public void AddTab(System.IO.DirectoryInfo dir)
        {
            _tabControl.Items.Add(new DXTabItem() { Header = dir.Name, Content = new ElementsControl(dir) });
        }

        public void CloseCurrentTab()
        {
            if(_tabControl.Items.Count > 1)
                _tabControl.RemoveTabItem(_tabControl.SelectedIndex);
        }

        public void SetOnStartTabs()
        {
            // TO DO: set check for previous session
            SetDefaultTabs();
        }

        public void SetDefaultTabs()
        {
            var dir = new System.IO.DirectoryInfo(DEFAULT_TAB_PATH);
            _tabControl.Items.Add(new DXTabItem() { Header = dir.Name, Content = new ElementsControl(dir)});
        }
    }
}
