﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace FileManager
{
    /// <summary>
    /// Interaction logic for ElementsControl.xaml
    /// </summary>
    public partial class ElementsControl : UserControl
    {
        DirectoryInfo _dir;
        public ElementsControl(DirectoryInfo dir)
        {
            _dir = dir;
            InitializeComponent();
            currentPath.Text = _dir.FullName;
            //how to avoid it?
            //TO DO : solve this problem
            statusBarTextBlock.Text = SelectionInfoString;
            ElementsListView.ItemsSource = WrapedElements;
        }
        Func<IElement, bool> filter = Filter;

        #region Properties

        private static string _startsWithFilter;
        public static string StartsWithFilter
        {
            get { return _startsWithFilter ?? string.Empty; }
            set { _startsWithFilter = value; }
        }

        private IEnumerable<IElement> _selectedElementsMemorized;

        public IEnumerable<IElement> SelectedElementsMemorized
        {
            get 
            {
                return _selectedElementsMemorized ?? new List<IElement>().DefaultIfEmpty(); 
            }
        }

        public DirectoryInfo Directory
        {
            get { return _dir; }
        }

        public ListView ElementsListView
        {
            get { return lvElements; }
        }

        public string SelectionInfoString
        {
            get
            {
                int files = FileElements.Count();
                int dirs = DirectoryElements.Count();
                int selectedFiles = SelectedFileElements.Count();
                int selectedDirs = SelectedDirectoryElements.Count();
                return string.Format("{6} {8} / {7} {8} {9} {0} / {1} {2}, {3} / {4} {5}", 
                                selectedFiles, files, "file(s)", selectedDirs, dirs, "dir(s)", 
                                SelectedFileElementsSizeKb.ToString("N0"), FileElementsSizeKb.ToString("N0"), "kb", "in");
            }
        }
        
        public DirectoryElement DirectoryElement
        {
            get { return new DirectoryElement(_dir); }
        }

        public IEnumerable<IElement> Elements
        {
            get { return DirectoryElement.GetElements().Where(filter); }
        }

        public IEnumerable<IElement> WrapedElements
        {
            get { return ElementManager.WrapElements(Elements.ToList(), _dir.Parent); }
        }

        public IEnumerable<FileElement> FileElements
        {
            get { return Elements.OfType<FileElement>(); }
        }

        public double FileElementsSizeKb
        {
            get 
            {
                double sizeBytes = 0;
                foreach (var e in FileElements)
                {
                    sizeBytes += e.File.Length;
                }
                return sizeBytes / 1024;
            }
        }

        public IEnumerable<DirectoryElement> DirectoryElements
        {
            get { return Elements.OfType<DirectoryElement>(); }
        }

        public IEnumerable<IElement> SelectedElements
        {
            get
            {
                return ElementsListView.SelectedItems.OfType<IElement>().Where(e => e.Simulated == false);
            }
        }

        public IEnumerable<FileElement> SelectedFileElements
        {
            get
            {
                return ElementsListView.SelectedItems.OfType<FileElement>().Where(e => e.Simulated == false);
            }
        }

        public double SelectedFileElementsSizeKb
        {
            get
            {
                double sizeBytes = 0;
                foreach (var e in SelectedFileElements)
                {
                    sizeBytes += e.File.Length;
                }
                return sizeBytes / 1024;
            }
        }

        public IEnumerable<DirectoryElement> SelectedDirectoryElements
        {
            get
            {
                return ElementsListView.SelectedItems.OfType<DirectoryElement>().Where(e => e.Simulated == false);
            }
        }

        public string CurrentPath
        {
            get
            {
                return Directory.FullName;
            }
            set
            {
                var dir = new DirectoryInfo(value);
                if (dir.Exists)
                {
                    ChangeDirectory(dir);
                }
            }
        }
        #endregion

        #region Methods

        private static bool Filter(IElement element)
        {
            return element.Name.StartsWith(StartsWithFilter, StringComparison.InvariantCultureIgnoreCase);
        }

        public void RefreshData()
        {
            ElementsListView.ItemsSource = WrapedElements;
            currentPath.Text = _dir.FullName;
        }

        private void ChangeDirectory(DirectoryInfo dir)
        {
            _dir = dir;
            RefreshData();
        }

        #endregion

        #region Events

        private void ElementsListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem item = sender as ListViewItem;
            object obj = item.Content;
            if (e.ChangedButton == MouseButton.Left)
            {
                if (obj != null && obj is DirectoryElement)
                {
                    try
                    {
                        ChangeDirectory((obj as DirectoryElement).Directory);
                    }
                    catch
                    {
                        ChangeDirectory(_dir = _dir.Parent);
                        MessageBox.Show("Виникла непередбачена помилка при відкритті теки.");
                    }
                }

                if (obj != null && obj is FileElement)
                {
                    try
                    {
                        System.Diagnostics.Process.Start((obj as FileElement).File.FullName);
                    }
                    catch
                    {
                        MessageBox.Show("Виникла непередбачена помилка при відкритті файлу.");
                    }
                }
            }
        }

        private void ElementsListViewItem_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListViewItem item = sender as ListViewItem;
            object obj = item.Parent;

            
        }

        private void ElementsListViewItem_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListViewItem item = sender as ListViewItem;
            object obj = item.Parent;


        }

        private void ElementsListView_LostFocus(object sender, RoutedEventArgs e)
        {
            //ElementsListView.UnselectAll();
        }

        private void ElementsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            statusBarTextBlock.Text = SelectionInfoString;
            _selectedElementsMemorized = SelectedElements;
        }

        private void lvElements_GotFocus(object sender, RoutedEventArgs e)
        {
            
        }

        #endregion

        private void ElementName_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 1 && e.ChangedButton == MouseButton.Left)
            {
                System.Timers.Timer timer = new System.Timers.Timer(2000);
                timer.Elapsed += (ss, ee) =>
                    {
                        if (sender == e.MouseDevice.DirectlyOver)
                        {
                            var stackPanel = sender as StackPanel;
                            //if(stackPanel != null)
                                
                        }
                    };
                timer.Start();
                
            }
        }

        private void filterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            StartsWithFilter = filterTextBox.Text;
            RefreshData();
            if (filterTextBox.Text == string.Empty)
                filterTextBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void filterTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (filterTextBox.Text == string.Empty)
                filterTextBox.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void lvElements_KeyDown(object sender, KeyEventArgs e)
        {
            if (!currentPath.IsFocused && !currentPath.IsEditorActive && Char.IsLetterOrDigit(e.Key.ToString()[0]))
            {
                //filterTextBox.Text += e.Key.ToString()[0];
                filterTextBox.Visibility = System.Windows.Visibility.Visible;
                filterTextBox.Focus();
            }
        }

        private void currentPath_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var textBox = e.OriginalSource as TextBox;
                if (textBox != null)
                {
                    if(System.IO.Directory.Exists(textBox.Text))
                        CurrentPath = currentPath.Text;
                }
            }
        }

        

        
    }
}
