﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;


namespace FileManager
{
    public class CustomCommands
    {
        PanelManager panelManager
        {
            get { return (App.Current.MainWindow as MainWindow).PanelManager; }
        }

        #region Commands

        public static readonly RoutedUICommand Exit = new RoutedUICommand
                        (
                                "Exit",
                                "Exit",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F4, ModifierKeys.Alt)
                                }
                        );
        public static readonly RoutedUICommand Refresh = new RoutedUICommand
                        (
                                "Refresh",
                                "Refresh",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.R, ModifierKeys.Control)
                                }
                        );

        public static readonly RoutedUICommand SwitchActivePanel = new RoutedUICommand
                        (
                                "SwitchActivePanel",
                                "SwitchActivePanel",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.Tab)
                                }
                        );

        public static readonly RoutedUICommand Rename = new RoutedUICommand
                        (
                                "Rename",
                                "Rename",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F2)
                                }
                        );

        public static readonly RoutedUICommand View = new RoutedUICommand
                        (
                                "View",
                                "View",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F3)
                                }
                        );

        public static readonly RoutedUICommand Edit = new RoutedUICommand
                        (
                                "Edit",
                                "Edit",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F4)
                                }
                        );

        public static readonly RoutedUICommand Copy = new RoutedUICommand
                        (
                                "Copy",
                                "Copy",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F5)
                                }
                        );

        public static readonly RoutedUICommand Move = new RoutedUICommand
                        (
                                "Move",
                                "Move",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F6)
                                }
                        );

        public static readonly RoutedUICommand NewFolder = new RoutedUICommand
                        (
                                "NewFolder",
                                "NewFolder",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F7)
                                }
                        );

        public static readonly RoutedUICommand NewEmptyDocument = new RoutedUICommand
                        (
                                "NewEmptyDocument",
                                "NewEmptyDocument",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.N, ModifierKeys.Control)
                                }
                        );

        public static readonly RoutedUICommand Delete = new RoutedUICommand
                        (
                                "Delete",
                                "Delete",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.F8),
                                        new KeyGesture(Key.Delete)
                                }
                        );

        public static readonly RoutedUICommand NewTab = new RoutedUICommand
                        (
                                "NewTab",
                                "NewTab",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.T, ModifierKeys.Control),
                                }
                        );

        public static readonly RoutedUICommand CloseCurrentTab = new RoutedUICommand
                        (
                                "CloseCurrentTab",
                                "CloseCurrentTab",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.W, ModifierKeys.Control),
                                }
                        );

        public static readonly RoutedUICommand GroupFilesByExtension = new RoutedUICommand
                        (
                                "GroupFilesByExtension",
                                "GroupFilesByExtension",
                                typeof(CustomCommands),
                                new InputGestureCollection()
                                {
                                        new KeyGesture(Key.G, ModifierKeys.Control),
                                }
                        );

        #endregion


    }
}
