﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager
{
    public class DirectoryElement : Element
    {
        private DirectoryInfo _directory;
        private string _alias;
        private bool _simulated;

        public DirectoryElement(DirectoryInfo dir)
        {
            _directory = dir;
        }

        #region Properties

        public override bool Simulated
        {
            get { return _simulated; }
            set { _simulated = value; }
        }

        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        public DirectoryInfo Directory
        {
            get { return _directory; }
        }

        public override bool Available
        {
            get { return PathExists(_directory.FullName); }
        }

        public override bool IsEditable
        {
            get { return true; }
        }

        public override string ImageSource
        {
            get 
            { 
                if (Simulated == false)
                    return IconsManager.GetImageSource(_directory);
                return null;
            }
        }

        public override string Name
        {
            get 
            {
                if (_alias != null) return _alias;
                return _directory.Name; 
            }
        }

        public override string Extension 
        {
            get { return string.Empty; }
        }

        public override string Size
        {
            get { return SIZE_DIRECTORY; }
        }

        public override FileAttributes Attributes
        {
            get { return _directory.Attributes; }
        }

        public override DateTime Date
        {
            get { return _directory.LastWriteTimeUtc; }
        }

        #endregion

        public List<IElement> GetElements()
        {
            List<IElement> elements = new List<IElement>();
            elements.AddRange(_directory.GetDirectories().Select(d => new DirectoryElement(d)));
            elements.AddRange(_directory.GetFiles().Select(f => new FileElement(f)));
            return elements;
        }

        public List<FileElement> GetFileElements()
        {
            List<FileElement> elements = new List<FileElement>();
            elements.AddRange(_directory.GetFiles().Select(f => new FileElement(f)));
            return elements;
        }

        public List<DirectoryElement> GetDirectoryElements()
        {
            List<DirectoryElement> elements = new List<DirectoryElement>();
            elements.AddRange(_directory.GetDirectories().Select(d => new DirectoryElement(d)));
            return elements;
        }

        public void CopyTo(string destDirName, bool copySubDirs = false)
        {
            Copy(_directory.FullName, destDirName, copySubDirs);
        }

        public static void Copy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!System.IO.Directory.Exists(destDirName))
            {
                System.IO.Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    Copy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public void MoveTo(string destDirName)
        {
            _directory.MoveTo(destDirName);
        }

        public void Delete()
        {
            _directory.Delete(true);
        }
    }
}
