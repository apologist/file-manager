﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager
{
    public class EditManager
    {
        public static void Open(FileInfo file)
        {
            try
            {
                if (!file.Exists)
                    throw new Exception("Файл, который вы пытаетесь открыть не существует");
                var textEditor = new SimpleTextEditor.MainWindow(file);
                textEditor.Show();
            }
            catch (Exception ex)
            {
                DevExpress.Xpf.Core.DXMessageBox.Show(ex.Message);
            }
        }
    }
}
