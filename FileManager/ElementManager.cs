﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileManager
{
    class ElementManager
    {
        public static IEnumerable<IElement> GetElementsFromDirectory(DirectoryInfo dir)
        {          
            var elements = new DirectoryElement(dir).GetElements();
            return WrapElements(elements.ToList(), dir.Parent);
        }

        public static List<IElement> WrapElements(List<IElement> elements, DirectoryInfo parentDir = null)
        {
            if (parentDir != null)
            {
                var element = new DirectoryElement(parentDir) { Alias = "[..]", Simulated =  true };
                elements.Insert(0, element);
            }
            return elements;
        }
    }
}
