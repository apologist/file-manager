﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager.Interfaces
{
    public interface ActiveManager
    {
        bool IsActive(object obj);
        void Activate(object obj);
    }
}
