﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    public interface IElement
    {
        bool Available { get;  }
        string Extension { get; }
        string Name { get;  }
        //using this for selection of elements in Control
        //TO DO : think about better way or another Property
        bool Simulated { get; set; }
        string Size { get; }
        DateTime Date { get;  }
        bool IsEditable { get; }
    }
}
