﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    class Session
    {
        private ItemCollection tabsPanel1;
        private ItemCollection tabsPanel2;
        public ItemCollection TabsPanel1
        {
            get { return tabsPanel1; }
            set { tabsPanel1 = value; }
        }
        public ItemCollection TabsPanel2
        {
            get { return tabsPanel2; }
            set { tabsPanel2 = value; }
        }
    }
}
