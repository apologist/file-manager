﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Reflection;

namespace FileManager
{
    class Localize
    {
        public static string GetString(string name)
        {
            return ResourcesManager.GetString(name, CurrentCulture);
            
        }

        private static CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
        public static CultureInfo CurrentCulture
        {
            get { return currentCulture; }
            set 
            {
                if (CultureInfo.GetCultures(CultureTypes.AllCultures).Contains(value))
                    currentCulture = value; 
            }
        }

        private static ResourceManager resourcesManager;
        public static ResourceManager ResourcesManager
        {
            get 
            {
                if (resourcesManager == null)
                    resourcesManager = new ResourceManager("FileManager.Properties.Resources", Assembly.GetExecutingAssembly());
                return resourcesManager;
            }
        }
    }
}
