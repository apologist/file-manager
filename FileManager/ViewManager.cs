﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using SimpleTextEditor;

namespace FileManager
{
    public class ViewManager
    {
        public static void Open(FileInfo file)
        {
            try
            {
                if (!file.Exists)
                    throw new Exception("Файл, который вы пытаетесь открыть не существует");
                SimpleTextEditor.MainWindow simpleTextEditor = new SimpleTextEditor.MainWindow(file, true);
                simpleTextEditor.Show();
                //System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo()
                //{
                //    Arguments = file.FullName + @" \readonly",
                //    FileName = @"SimpleTextEditor\SimpleTextEditor.exe"
                //};
                //System.Diagnostics.Process.Start(startInfo);
            }
            catch (Exception ex)
            {
                DevExpress.Xpf.Core.DXMessageBox.Show(ex.Message);
            }

        }
    }
}
