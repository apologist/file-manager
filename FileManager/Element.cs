﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.IO;

namespace FileManager
{
    public abstract class Element : IElement
    {
        protected const string SIZE_DIRECTORY = "<DIR>";

        public abstract bool Available { get; }

        public abstract bool Simulated { get; set; }

        public abstract bool IsEditable { get; }

        public abstract string ImageSource { get; }

        public abstract string Name { get; }

        public abstract string Extension { get; }

        public abstract string Size { get; }

        public abstract FileAttributes Attributes { get; }

        public abstract DateTime Date { get; }


        protected static bool PathExists(string path)
        {
            return (Directory.Exists(path) || File.Exists(path));
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
