﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Reflection;
using DevExpress.Utils.Controls;
using DevExpress.Xpf.Core;
using io = System.IO;
using System.ComponentModel;

namespace FileManager
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        PanelManager panelManager;
        public MainWindow()
        {
            InitializeComponent();
            panelManager = new PanelManager(panel1, panel2);
            panel1.Focus();
            drivePanel1.SelectedItem = new io.DriveInfo("C:\\");
            drivePanel2.SelectedItem = new io.DriveInfo("C:\\");new io.DriveInfo("C:\\");
            panelManager.TabManager1.TabControl.ManipulationCompleted += (s, e) => 
            { 
                var dr = Drives.Where(d => d.RootDirectory.Root.Name.Equals(Panel1DriveName)).FirstOrDefault();
                drivePanel1.SelectedItem = dr;
            };
            panelManager.TabManager2.TabControl.ManipulationCompleted += (s, e) => 
            {
                drivePanel2.SelectedItem = Drives.Where(d => d.RootDirectory.Root.Name.Equals(Panel2DriveName)).FirstOrDefault(); 
            };
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public PanelManager PanelManager
        {
            get { return panelManager; }
        } 

        public List<io.DriveInfo> Drives
        {
            get { return io.DriveInfo.GetDrives().ToList(); }
        }

        public string Panel1DriveName
        {
            get { return panelManager.TabManager1.ActiveElementsControl.Directory.Root.Name; }
        }

        public string Panel2DriveName
        {
            get { return panelManager.TabManager2.ActiveElementsControl.Directory.Root.Name; }
        }

        private io.DriveInfo _drivePanel1;
        public io.DriveInfo DrivePanel1
        {
            get 
            {
                try
                {
                    return _drivePanel1 ?? Drives.Where(d => d.RootDirectory.Root.Name.Equals(Panel1DriveName)).FirstOrDefault();
                }
                catch { return null; }
            }
            set 
            {
                //drivePanel1.SelectedItem = value;
                _drivePanel1 = value;
                if (drivePanel1.SelectedItem != null && !drivePanel1.SelectedItem.Equals(value))
                {
                    panelManager.TabManager1.ActiveElementsControl.CurrentPath = value.Name;
                }
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("DrivePanel1"));

            }
        }

        private io.DriveInfo _drivePanel2;
        public io.DriveInfo DrivePanel2
        {
            get 
            {
                try
                {
                    return _drivePanel2 ?? Drives.Where(d => d.RootDirectory.Root.Name.Equals(Panel1DriveName)).FirstOrDefault();
                }
                catch { return null; }
            }
            set 
            {
                _drivePanel2 = value;
                if (drivePanel2.SelectedItem != null && !drivePanel2.SelectedItem.Equals(value))
                {
                    panelManager.TabManager2.ActiveElementsControl.CurrentPath = value.Name;
                }
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("DrivePanel2"));
            }
        }
        

        #region Commands

        private void ExitCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ExitCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void RefreshCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RefreshCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            panelManager.RefreshActiveViews();
        }

        private void SwitchActivePanelCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void SwitchActivePanelCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            panelManager.SwitchActivePanel();
        }

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            System.IO.DirectoryInfo dir = panelManager.InActiveElementsControl.Directory;
            var elements = panelManager.ActiveElementsControl.SelectedElementsMemorized;
            foreach (var element in elements)
            {
                if (element is FileElement)
                {
                    //TO DO : ask if file overwrites
                    (element as FileElement).CopyTo(System.IO.Path.Combine(dir.FullName, element.Name), true);
                }
                if (element is DirectoryElement)
                {
                    //TO DO : ask if directory overwrites
                    (element as DirectoryElement).CopyTo(System.IO.Path.Combine(dir.FullName, element.Name), true);
                }
            }
            panelManager.RefreshActiveViews();
        }

        private void Move_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            System.IO.DirectoryInfo dir = panelManager.InActiveElementsControl.Directory;
            var elements = panelManager.ActiveElementsControl.SelectedElementsMemorized;
            foreach (var element in elements)
            {
                if (element is FileElement)
                {
                    //TO DO : ask if file overwrites
                    (element as FileElement).MoveTo(System.IO.Path.Combine(dir.FullName, element.Name));
                }
                if (element is DirectoryElement)
                {
                    //TO DO : ask if directory overwrites
                    (element as DirectoryElement).MoveTo(System.IO.Path.Combine(dir.FullName, element.Name));
                }
            }
            panelManager.RefreshActiveViews();
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            System.IO.DirectoryInfo dir = panelManager.InActiveElementsControl.Directory;
            var elements = panelManager.ActiveElementsControl.SelectedElementsMemorized;
            foreach (var element in elements)
            {
                if (element is FileElement)
                {
                    //TO DO : ask if file overwrites
                    (element as FileElement).Delete();
                }
                if (element is DirectoryElement)
                {
                    //TO DO : ask if directory overwrites
                    (element as DirectoryElement).Delete();
                }
            }
            panelManager.RefreshActiveViews();
        }

        private void NewFolder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var currentDirectory = panelManager.ActiveElementsControl.Directory;
            var dir = new System.IO.DirectoryInfo(System.IO.Path.Combine(currentDirectory.FullName, "Новая папка"));
            int number = 0;
            while (dir.Exists)
            {
                number++;
                string addition = (number == 0) ? string.Empty : string.Format("({0})", number);
                dir = new System.IO.DirectoryInfo(System.IO.Path.Combine(currentDirectory.FullName, "Новая папка" + addition));
            }
            panelManager.ActiveElementsControl.Directory.CreateSubdirectory(dir.Name);
            panelManager.RefreshActiveViews();
        }

        private void NewEmptyDocument_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var currentDirectory = panelManager.ActiveElementsControl.Directory;
            var file = new System.IO.FileInfo(System.IO.Path.Combine(currentDirectory.FullName, "Новый документ"));
            int number = 0;
            while (file.Exists)
            {
                number++;
                string addition = (number == 0) ? string.Empty : string.Format("({0})", number);
                file = new System.IO.FileInfo(System.IO.Path.Combine(currentDirectory.FullName, "Новый документ" + addition));
            }
            System.IO.File.Create(file.FullName);
            panelManager.RefreshActiveViews();
        }

        private void Rename_Executed(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void View_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var elements = panelManager.ActiveElementsControl.SelectedElementsMemorized;
            foreach (var element in elements)
            {
                if (element is FileElement)
                {
                    ViewManager.Open((element as FileElement).File);
                }
            }
            panelManager.RefreshActiveViews();
        }

        private void Edit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var elements = panelManager.ActiveElementsControl.SelectedElementsMemorized;
            foreach (var element in elements)
            {
                if (element is FileElement)
                {
                    EditManager.Open((element as FileElement).File);
                }
            }
            panelManager.RefreshActiveViews();
        }

        private void NewTab_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            panelManager.GetActiveTabManager().AddTab(panelManager.GetActiveTabManager().ActiveElementsControl.Directory);
        }

        private void CloseCurrentTab_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            panelManager.GetActiveTabManager().CloseCurrentTab();
        }

        private void GroupFilesByExtension_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            var dir = panelManager.ActiveElementsControl.Directory;
            var fileElements = panelManager.ActiveElementsControl.DirectoryElement.GetFileElements();
            var groups = fileElements.Select(f => f.Extension.ToUpper()).Distinct();
            foreach (var group in groups)
            {
                var newDir = new io.DirectoryInfo(io.Path.Combine(dir.FullName, group));
                int num = 1;
                while(newDir.Exists)
                {
                    num++;
                    newDir = new io.DirectoryInfo(io.Path.Combine(dir.FullName, string.Format("{0}({1})", group, num)));
                }
                newDir.Create();
                foreach(var file in dir.GetFiles().Where(f => f.Extension.ToUpper().Equals(group)))
                {
                    file.MoveTo(io.Path.Combine(newDir.FullName, file.Name));
                }
            }
            panelManager.RefreshActiveViews();
        }

        #endregion
    }
}
