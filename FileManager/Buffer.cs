﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileManager
{
    public static class Buffer
    {
        private static List<IElement> _elements;

        public static void Push(List<IElement> elements)
        {
            _elements = elements;
        }

        public static List<IElement> Pull()
        {
            return _elements;
        }
    }
}
