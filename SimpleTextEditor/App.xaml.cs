﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;

namespace SimpleTextEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        void App_Startup(object sender, StartupEventArgs e)
        {
            bool isReadOnly = false;
            foreach (string arg in e.Args)
            {
                if (arg.ToUpper() == @"\READONLY")
                    isReadOnly = true;
            }
            System.IO.FileInfo file = new System.IO.FileInfo(System.IO.Path.Combine(System.IO.Path.GetTempPath(), @"Новый документ"));
            if (file.Exists) file.Delete(); file.Create();
            if (e.Args[0] != null && System.IO.File.Exists(e.Args[0]))
                file = new System.IO.FileInfo(e.Args[0]);
            MainWindow mainWindow = new MainWindow(file, isReadOnly);
            mainWindow.Show();
        }
    }
}
