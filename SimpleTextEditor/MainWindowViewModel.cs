﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Microsoft.Win32;
using System.IO;
using System.Windows.Controls;

namespace SimpleTextEditor
{
    public class MainWindowViewModel : BaseViewModel
    {

        public MainWindowViewModel()
        {
            _isReadOnly = false;
        }

        public MainWindowViewModel(bool isReadOnly, FileInfo file)
        {
            _isReadOnly = isReadOnly;
            if (file.Exists)
                _fileInfo = file;
            else
            {
                _fileInfo = new FileInfo(Path.GetTempFileName());
            }
        }

        #region Properties

        private FileInfo _fileInfo;
        public FileInfo FileInfo
        {
            get { return _fileInfo; }
            set 
            { 
                _fileInfo = value;
                RaisePropertyChanged(() => FileInfo);
                RaisePropertyChanged(() => Title);
                RaisePropertyChanged(() => Text);
            }
        }

        private FileInfo NewRandomFile
        {
            get { return new FileInfo(Path.GetTempFileName()); }
        }

        private string _text;
        public string Text
        {
            get 
            {
                try
                {
                    return File.ReadAllText(FileInfo.FullName);
                }
                catch (Exception ex)
                {
                    DevExpress.Xpf.Core.DXMessageBox.Show(ex.Message);
                    FileInfo = NewRandomFile;
                    return string.Empty;
                }
            }
            set
            {
                _text = value;
            }
        }
        
        private bool _isReadOnly;
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; }
        }

        public string Title
        {
            get { return "Текстовий редактор - " + FileInfo.Name; }
        }

        #endregion

        #region Commands

        public ICommand NewFileCommand
        {
            get
            { return new DelegateCommand(() => NewFile()); }
        }

        public ICommand OpenFileCommand
        {
            get { return new DelegateCommand(() => OpenFile()); }
        }

        public ICommand CloseCommand
        {
            get
            { return new DelegateCommand(() => CloseFile()); }
        }

        public ICommand SaveCommand
        {
            get
            { return new DelegateCommand(() => Save()); }
        }

        public ICommand SaveAsCommand
        {
            get
            { return new DelegateCommand(() => SaveAs()); }
        }

        public ICommand ExitCommand
        {
            get
            { return new DelegateCommand(() => Exit()); }
        }

        #endregion

        #region Methods

        private void NewFile()
        {
            Save();
            FileInfo = NewRandomFile;
        }

        private void OpenFile()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;
            if (openFileDialog.ShowDialog() == true)
                FileInfo = new FileInfo(openFileDialog.FileName);
        }

        private void CloseFile()
        {
            Save();
            FileInfo = NewRandomFile;
        }

        private void Save()
        {
            File.WriteAllText(FileInfo.FullName, _text, Encoding.UTF8);
        }

        private void SaveAs()
        {
            var saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.InitialDirectory = Environment.CurrentDirectory;
            if (saveFileDialog.ShowDialog() == true)
                File.WriteAllText(saveFileDialog.FileName, _text, Encoding.UTF8);

        }

        private void Exit()
        {
            App.Current.Windows.OfType<MainWindow>().Where(w => w.Title == this.Title).FirstOrDefault().Close();
        }

        #endregion

    }
}
