﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DevExpress.Xpf.Core;
using DevExpress.Xpf.Bars;
using System.IO;


namespace SimpleTextEditor
{
    public partial class MainWindow : DXWindow
    {
        public MainWindow()
        {
            DataContext = new MainWindowViewModel();
            InitializeComponent();
        }

        public MainWindow(FileInfo file, bool isReadOnly = false)
        {
            DataContext = new MainWindowViewModel(isReadOnly, file);
            InitializeComponent();
        }
    }


}
